#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on Jan 3, 2015

@author: kulpanov
'''

if __name__ == '__main__':
    except_occ = False
    try:
        main(layer=2)
    except:
        except_occ = True
        raise
    
    finally:
        # Sory for this rude thing, but the program doesn't stop by itself
        if not except_occ:
            import signal
            os.kill(os.getpid(), signal.SIGTERM)

    pass