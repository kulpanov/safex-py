# import MySQLdb
import mysql
from mysql.connector import errorcode

DEF_SQL_PASS = "chemin"
DEF_SQL_USER = "administrator"
  
'''
Created on Jan 13, 2015

@author: kulpanov
'''

class CMySQLClient(object):
  '''
  classdocs
  '''
  mTimeStamp = 0
  mSens1 = 0
  mSens2 = 0
  mSens3 = 0
  mEmail = 0
  mShutdown = 0
  mStop = 0        
  mValveH = 0      
  mValveN = 0     
  mSaveH = 0
  eStop = 0
    
  def pull_Initialize(self):
    ;
    
  def push_Initialize(self):
  
  def setEStop(self):
  
  def getData_H1(self):  
  
  def addData_H1(self):  
    
  def getData_H2(self):  
  
  def addData_H2(self):  
  
  def getData_H3(self):  
  
  def replaceData_H3(self):  
  
  def release(self):
    self.db.close()

   
  def connect(self):
    # self.db = MySQLdb.connect(host="localhost", user=self.DEF_SQL_USER, passwd=self.DEF_SQL_PASS, db="safex")
    try:
      self.db = mysql.connector.connect(user=DEF_SQL_USER, password=DEF_SQL_PASS,
                              host='127.0.0.1',
                              database='safex')
    
    except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
      print("Access denied")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
      print("Database does not exists")
    else:
      print(err)
    self.db.close()
  
              
  def __init__(self, db):
      '''
      Constructor
      '''
    self.connect()
 
def recreateDB():
  try:
    db = mysql.connector.connect(user=DEF_SQL_USER, password=DEF_SQL_PASS,
                              host='127.0.0.1')
    cursor = db.cursor()

    cursor.execute("DROP DATABASE safex;")
    
    cursor.execute(
          "CREATE DATABASE safex DEFAULT CHARACTER SET 'utf8'")
      
    cursor.execute("CREATE TABLE initialize(idnr INTEGER AUTO_INCREMENT PRIMARY KEY"
      ", time_stamp DATETIME"
      ", sens_1 BOOL NOT NULL DEFAULT 1, sens_2 BOOL NOT NULL DEFAULT 1, sens_3 BOOL NOT NULL DEFAULT 0"
      ", e_mail BOOL NOT NULL DEFAULT 0, c_shutdown BOOL NOT NULL DEFAULT 0, c_stop BOOL NOT NULL DEFAULT 0"
      ", valve_h BOOL NOT NULL DEFAULT 0, valve_n BOOL NOT NULL DEFAULT 0, save_h BOOL NOT NULL DEFAULT 1"
      ", emergency_stop BOOL DEFAULT 0"
      ");")

    cursor.execute("CREATE TABLE H_1(idnr INTEGER AUTO_INCREMENT PRIMARY KEY, time_stamp DATETIME"
      ", value INTEGER, temperature INTEGER);")
    
    cursor.execute("CREATE TABLE H_2(idnr INTEGER AUTO_INCREMENT PRIMARY KEY, time_stamp DATETIME"
      ", value INTEGER);")
    
    cursor.execute("CREATE TABLE H_3(idnr INTEGER AUTO_INCREMENT PRIMARY KEY, time_stamp DATETIME"
      ", value INTEGER);")
    
  except mysql.connector.Error as err:
      print("Failed recreating database: {}".format(err))
      exit(1)

    
  dbClient = CMySQLClient()
  dbClient.push_Initialize();
  dbClient.addData_H1(0, 0)
  dbClient.addData_H2(0)
  dbClient.replaceData_H3(0)
 

 
    
    
